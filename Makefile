SERVICE=work
CMD=docker-compose

all: help

setup:
	go get github.com/Songmu/make2help/cmd/make2help

## start
start:
	$(CMD) run --rm $(SERVICE)

## ヘルプを表示
help:
	@make2help $(MAKEFILE_LIST)
